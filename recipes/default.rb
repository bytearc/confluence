#LICENSE_NOTICE#

settings = Confluence.settings(node)
confluence_version = Chef::Version.new(node[:confluence][:version])
#Chef::Log.info("Installer : " + settings[:installer][:final_url])
include_recipe "apt"
#include_recipe "runit"

#configure the system user and group
#group node[:confluence][:group]

user node[:confluence][:user] do
  comment "Confuence Service Account"
  home    node[:confluence][:home]
  shell   "/bin/bash"
  #supports :manage_home => true
  #system  true
  action  :create 
end

#install java
include_recipe "java"

#install database
if settings[:database][:host] == "localhost"
  database_connection = {
    :host => settings[:database][:host],
    :port => settings[:database][:port]
  }

  case settings[:database][:type]
  when "mysql"
    include_recipe "mysql::server"
    include_recipe "database::mysql"
    database_connection.merge!({ :username => 'root', :password => node[:mysql][:server_root_password] })
    
    mysql_database settings[:database][:name] do
      connection database_connection
      collation "utf8_bin"
      encoding "utf8"
      action :create
    end

    # See this MySQL bug: http://bugs.mysql.com/bug.php?id=31061
    mysql_database_user "" do
      connection database_connection
      host "localhost"
      action :drop
    end

    mysql_database_user settings[:database][:user] do
      connection database_connection
      host "%"
      password settings[:database][:password]
      database_name settings[:database][:name]
      action [:create, :grant]
    end
  when "postgresql"
    include_recipe "postgresql::server"
    include_recipe "database::postgresql"
    database_connection.merge!({ :username => 'postgres', :password => node[:postgresql][:password][:postgres] })
    
    postgresql_database settings[:database][:name] do
      connection database_connection
      connection_limit "-1"
      encoding "utf8"
      action :create
    end

    postgresql_database_user settings[:database][:user] do
      connection database_connection
      password settings[:database][:password]
      database_name settings[:database][:name]
      action [:create, :grant]
    end
  end
end


remote_file "#{Chef::Config[:file_cache_path]}/#{node[:confluence][:installer][:file]}" do
  source    settings[:installer][:final_url]
  #checksum  node[:stash][:checksum]
  mode      "0644"
  action    :create_if_missing
end

execute "Extracting Confluence #{node[:confluence][:version]}" do
  cwd Chef::Config[:file_cache_path]
  command <<-COMMAND
    tar -zxf #{node[:confluence][:installer][:file]}
    mkdir -p #{node[:confluence][:install_basepath]}
    mv #{node[:confluence][:installer][:file_name]} #{node[:confluence][:install_basepath]}
    chown -R #{node[:confluence][:user]} #{node[:confluence][:install_path]}
  COMMAND
  creates "#{node[:confluence][:install_path]}"
end

#doubtful, need cleanup
if settings[:database][:type] == "mysql"
  directory "#{node[:confluence][:install_path]}/confluence/WEB-INF/lib" do
    owner node[:confluence][:user]
    group node[:confluence][:user]
    mode 00755
    action :create
  end
  
  #mysql_connector_j "#{node[:confluence][:install_path]}/confluence/WEB-INF/lib"
  
  remote_file "#{Chef::Config[:file_cache_path]}/#{node[:mysql_connector][:j][:tar_file]}" do
    source node[:mysql_connector][:j][:url]
    checksum node[:mysql_connector][:j][:checksum]
    mode  "0644"
    action :create_if_missing
  end

  execute "Extracting mysql-connector-java-#{node[:mysql_connector][:j][:version]}" do
    cwd Chef::Config[:file_cache_path]
    command <<-COMMAND
      tar --strip-components=1 -zxf #{node[:mysql_connector][:j][:tar_file]}
      mv #{node[:mysql_connector][:j][:jar_file]} #{node[:confluence][:install_path]}/confluence/WEB-INF/lib/#{node[:mysql_connector][:j][:jar_file]}
      chown #{node[:confluence][:user]} #{node[:confluence][:install_path]}/confluence/WEB-INF/lib/#{node[:mysql_connector][:j][:jar_file]}
    COMMAND
    creates "#{node[:confluence][:install_path]}/confluence/WEB-INF/lib/#{node[:mysql_connector][:j][:jar_file]}"
  end
end

#directory "#{node[:confluence][:install_path]}" do
#  recursive true
#  owner node[:confluence][:run_user]
#  notifies :run, "execute[configure file permissions]", :immediately
#end

#cookbook_file "#{node[:confluence][:install_path]}/bin/startup.sh" do
#  owner node[:confluence][:user]
#  source "startup.sh"
#  mode 0755
#end

directory node[:confluence][:data] do
  owner node[:confluence][:user]
  group node[:confluence][:group]
  action :create
end

template "#{node[:confluence][:install_path]}/conf/server.xml" do
  owner node[:confluence][:user]
  source "server.xml.erb"
  mode 0644
end
#  
#template "#{node[:confluence][:install_path]}/atlassian-confluence/WEB-INF/classes/entityengine.xml" do
#  owner node[:confluence][:run_user]
#  source "entityengine.xml.erb"
#  mode 0644
#end
#

template "#{node[:confluence][:install_path]}/confluence/WEB-INF/classes/confluence-init.properties" do
  owner node[:confluence][:user]
  source "confluence-init.properties.erb"
  mode 0644
end 

if settings[:jvm][:java_opts] != ""
  template "#{node[:confluence][:install_path]}/bin/setenv.sh" do
    owner node[:confluence][:user]
    source "setenv.erb"
    mode 0644
  end
end  

service "confluence" do
  supports :restart => true, :start => true, :stop => true, :reload => false
  action :nothing
end 

template "confluence" do
  path "/etc/init.d/confluence"
  source "confluence-service.erb"
  owner "#{node[:confluence][:user]}"
  group "#{node[:confluence][:group]}"
  mode "0755"
  notifies :enable, "service[confluence]"
  notifies :start, "service[confluence]"
end
#runit_service "confluence"