class Chef::Recipe::Confluence
  def self.settings(node)
    begin
      if Chef::Config[:solo]
        begin 
          settings = Chef::DataBagItem.load("confluence","confluence")['local']
        rescue
          Chef::Log.info("No stash data bag found")
        end
      else
        begin 
          settings = Chef::EncryptedDataBagItem.load("confluence","confluence")[node.chef_environment]
        rescue
          Chef::Log.info("No stash encrypted data bag found")
        end
      end
    ensure
      settings ||= node[:confluence]

      case settings[:database][:type]
      when "mysql"
        settings[:database][:port] ||= 3306
      when "postgresql"
        settings[:database][:port] ||= 5432
      else
        Chef::Log.warn("Unsupported database type.")
      end
    end

    settings
  end
end