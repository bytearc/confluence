#LICENSE_NOTICE#

set['build_essential']['compiletime'] = true

# The openssl cookbook supplies the secure_password library to generate random passwords
::Chef::Node.send(:include, Opscode::OpenSSL::Password)

default[:confluence][:version]                      =  "5.1.2" #"4.3.7"
default[:confluence][:user]                         = "confluence"
default[:confluence][:group]                        = "confluence"
default[:confluence][:home]                         = "/home/confluence"

default[:confluence][:installer][:url_base]         = "http://www.atlassian.com/software/confluence/downloads/binary"
default[:confluence][:installer][:file_name]        = "atlassian-confluence-#{node[:confluence][:version]}"
default[:confluence][:installer][:file_ext]         = "tar.gz"
default[:confluence][:installer][:file]             = "#{node[:confluence][:installer][:file_name]}.#{node[:confluence][:installer][:file_ext]}"
default[:confluence][:installer][:final_url]        = "#{node[:confluence][:installer][:url_base]}/#{node[:confluence][:installer][:file]}"

default[:confluence][:install_basepath]             = "/opt/atlassian"
default[:confluence][:install_path]                 = "#{node[:confluence][:install_basepath]}/#{node[:confluence][:installer][:file_name]}"
default[:confluence][:data]                         = "#{node[:confluence][:home]}"
default[:confluence][:port]                         = "8090"
default[:confluence][:baseurl]                      = "/confluence"

#Java
set[:java][:install_flavor]                         = "oracle"
set[:java][:java_home]                              = "/usr/lib/jvm/java-7-oracle"
set[:java][:jdk_version]                            = "7"
set[:java][:oracle][:accept_oracle_download_terms]  = true

#DB
default[:confluence][:database][:host]     = "localhost"
default[:confluence][:database][:name]     = "confluence"
default[:confluence][:database][:password] = "changeit"
default[:confluence][:database][:port]     = 3306
default[:confluence][:database][:type]     = "mysql"
default[:confluence][:database][:user]     = "confluence"

#DB-MYSQL-Required
set['mysql']['bind_address'] = "127.0.0.1"
set['mysql']['port']  = "#{node[:confluence][:database][:port]}"
set['mysql']['tunable']['collation-server']  = "utf8_bin"
set['mysql']['tunable']['max_allowed_packet'] = "32M"

#http://community.opscode.com/cookbooks/mysql/source - has tunable params for my.cnf

#https://github.com/opscode-cookbooks/mysql#chef-solo-note - needed for solo
set['mysql']['server_root_password'] = "changeit"
set['mysql']['server_debian_password'] = "changeit"
set['mysql']['server_repl_password'] = "changeit"

default[:mysql_connector][:j][:install_paths] = []
default[:mysql_connector][:j][:version]  = "5.1.24"
default[:mysql_connector][:j][:jar_file] = "mysql-connector-java-#{node[:mysql_connector][:j][:version]}-bin.jar"
default[:mysql_connector][:j][:tar_file] = "mysql-connector-java-#{node[:mysql_connector][:j][:version]}.tar.gz"
default[:mysql_connector][:j][:url]      = "http://cdn.mysql.com/Downloads/Connector-J/#{node[:mysql_connector][:j][:tar_file]}"
default[:mysql_connector][:j][:checksum] = "770ba1db73f7031272b8feaa3c78ee470e72d085f95f5f019852ad92eb634bcc"

#JVM
default[:confluence][:jvm][:java_opts]        = "-Xms512m -Xmx512m -XX:MaxPermSize=256m"
default[:confluence][:jvm][:support_args]     = ""

#Tomcat
default[:confluence][:tomcat][:keyAlias]      = "tomcat"
default[:confluence][:tomcat][:keystoreFile]  = "#{node[:confluence][:home]}/.keystore"
default[:confluence][:tomcat][:keystorePass]  = "changeit"
default[:confluence][:tomcat][:port]          = "8000"
default[:confluence][:tomcat][:ssl_port]      = "8443"