#LICENSE_NOTICE#

default[:confluence][:apache2][:access_log] = ""
default[:confluence][:apache2][:error_log] = ""
default[:confluence][:apache2][:port] = 80
default[:confluence][:apache2][:virtual_host_alias] = "confluence.#{domain}"
default[:confluence][:apache2][:virtual_host_name] = "confluence.#{domain}"
#default[:confluence][:apache2][:virtual_host_alias] = node['fqdn']
#default[:confluence][:apache2][:virtual_host_name] = node['hostname']

default[:confluence][:apache2][:ssl][:access_log] = ""
default[:confluence][:apache2][:ssl][:chain_file] = ""
default[:confluence][:apache2][:ssl][:error_log] = ""
default[:confluence][:apache2][:ssl][:port] = 443
default[:confluence][:apache2][:ssl][:certificate_file] = "/etc/ssl/certs/ssl-cert-snakeoil.pem"
default[:confluence][:apache2][:ssl][:key_file] = "/etc/ssl/private/ssl-cert-snakeoil.key"

#default[:confluence][:run_user]          = "www-data"
#default[:confluence][:server_port]       = "8001"
#default[:confluence][:connector_port]    = "8081"



