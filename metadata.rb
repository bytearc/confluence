name              "confluence"
maintainer        "budha"
maintainer_email  "budhash@gmail.com"
license           "Apache 2.0"
description       "Installs/Configures confluence"
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           "0.2.0"
depends           "apt"
depends           "openssl"
depends           "java"
depends           "apache2"
depends			      "mysql"
depends           "mysql_connector"
depends           "database"

%w{ ubuntu }.each do |os|
  supports os
end

%w{ apt openssl database java mysql mysql_connector postgresql }.each do |cb|
  depends cb
end


